import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private localNotifications: LocalNotifications) {

  }

  openNotification(){
    this.localNotifications.schedule({
      title: 'Title',
      text: 'Single ILocalNotification',
      data: { secret: 'hola' }
    });

    this.localNotifications.schedule({
      title: 'Title 2',
      text: 'Single ILocalNotification',
      trigger: {at: new Date(new Date().getTime() + 5000)},
      data: { secret: 'hola' }
    });
  }

}
